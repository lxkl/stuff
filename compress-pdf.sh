#!/bin/sh
# if unsure, use -r150
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress \
   -dNOPAUSE -dQUIET -dBATCH -dDetectDuplicateImages \
   -dCompressFonts=true -r"${1?}" -sOutputFile="${3?}" "${2?}"
