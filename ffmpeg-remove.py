## usage: python3 ffmpeg-remove.py INFILE START END OUTFILE
## START and END timestamps given in HH:MM:SS
##
## CRF for re-coding is set to 16 to preserve quality

import datetime, subprocess, sys

def time_to_seconds(time_str):
	for fmt in ["%H:%M:%S", "%M:%S", "%S"]:
		try:
			time_obj = datetime.datetime.strptime(time_str, fmt).time()
			return time_obj.hour * 3600 + time_obj.minute * 60 + time_obj.second
		except ValueError:
			pass
	return None

infile        = sys.argv[1]
begin_seconds = time_to_seconds(sys.argv[2])
end_seconds   = time_to_seconds(sys.argv[3])
outfile       = sys.argv[4]

filter_str = f"""[0:v]trim=start=0:end={begin_seconds},setpts=PTS-STARTPTS[v1];\
[0:v]trim=start={end_seconds},setpts=PTS-STARTPTS[v2];\
[0:a]atrim=start=0:end={begin_seconds},asetpts=PTS-STARTPTS[a1];\
[0:a]atrim=start={end_seconds},asetpts=PTS-STARTPTS[a2];\
[v1][a1][v2][a2]concat=n=2:v=1:a=1[outv][outa]"""

subprocess.run(["ffmpeg", "-i", infile,
				"-filter_complex", filter_str,
				"-map", "[outv]", "-map", "[outa]",
				"-c:v", "libx264", "-crf", "16", "-c:a", "aac",
				outfile],
			   text=True, check=True)
