#!/bin/env gosh
(use srfi-27)
(use file.util)
(use gauche.process)
(define (main args)
  (sys-sleep 3)
  (string-for-each
   (lambda (char)
     (sys-nanosleep (* 3e8 (random-real)))
     (do-process `("xdotool" "type" ,char)))
   (file->string (ref args 1))))
