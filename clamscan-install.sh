#!/bin/sh
set -e
sudo apt-get -y install clamav
sudo systemctl stop clamav-freshclam.service
sudo freshclam
