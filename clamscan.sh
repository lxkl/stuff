#!/bin/sh
set -e
opt_size_=5000m
opt_recursion_=10000
opt_files_=100000
clamscan -i \
         -l clamscan.log \
         --phishing-scan-urls=yes \
         --alert-exceeds-max=yes \
         --max-scantime=0 \
         --max-filesize="${opt_size_?}" \
         --max-scansize="${opt_size_?}" \
         --max-recursion="${opt_recursion_?}" \
         --max-files="${opt_files_?}" \
         -r "${1?}"
code_="$?"
echo "==> clamscan exit code: ${code_?}"
