#!/usr/bin/env gosh
(use file.util)
(use gauche.process)
(define *default* '())
(define (get-config item kw) (get-keyword* kw item (get-keyword kw *default*)))
(define (sftp item cmd)
  `("lftp" "-u" ,(format "~a," (get-config item :sftp-username)) "-e"
    ,(format #f "set sftp:connect-program 'ssh -o ConnectTimeout=30 -a -x'; set net:timeout 30; set net:max-retries 3; ~a; exit" cmd)
    ,(format #f "sftp://~a" (get-config item :sftp-server))))
(define (list-source item)
  (let ((type (get-config item :source-type))
        (path (get-config item :source-path)))
    (cond
     ((and (string=? type "sftp") (do-process (sftp item (format #f "cd ~a" path))))
      (process-output->string-list (sftp item (format #f "cd ~a; cls -1" path))))
     ((string=? type "sftp") (format #t "WARNING: network problem or no such directory: ~a~%" path) '())
     ((string=? type "local") (directory-list (expand-path path) :children? #t))
     (else (error "unknown source type:" item)))))
(define (get-file item s f)
  (let ((type (get-config item :source-type))
        (full-path (build-path (get-config item :source-path) s)))
    (cond
     ((string=? type "sftp") (do-process! (sftp item (format "get ~a -o ~a" full-path f))))
     ((string=? type "local") (copy-file (expand-path full-path) f))
     (else (error "unknown source type:" item)))))
(define (cut-and-recompress item s t)
  (call-with-temporary-directory
   (^d
    (let* ((ext (path-extension s))
           (file-in-0 (build-path d (format #f "in-0.~a" ext)))
           (file-in (build-path d (format #f "in.~a" ext)))
           (file-cut (build-path d (format #f "cut.~a" ext)))
           (file-recomp (build-path d (format #f "recomp.~a" ext))))
      (make-directory* (sys-dirname t))
      (format #f "getting copy of file...")
      (get-file item s file-in-0)
      (sys-sleep 30)
      (get-file item s file-in)
      (cond
       ((do-process `("cmp" ,file-in-0 ,file-in))
        (do-process! (list (expand-path (get-config item :auto-editor)) file-in
                           "--margin" (get-config item :margin)
                           "--edit" (format #f "audio:threshold=~a" (get-config item :threshold))
                           "--no-open"
                           "--output" file-cut))
        (do-process! (list "ffmpeg"
                           "-i" file-cut
                           "-c:v" "libx264"
                           "-crf" (get-config item :crf)
                           "-c:a" "copy"
                           file-recomp))
        (move-file file-recomp t))
       (else (format #t "file is changing, try again later")))))
   :directory (expand-path (get-config item :temp-dir)) :prefix "auto-editor-batch-"))
(define (process-item item)
  (when (get-config item :active?)
    (format #t "====> ~a~%" (get-config item :description))
    (format #t "      ~a (~a) --> ~a~%"
            (get-config item :source-path)
            (get-config item :source-type)
            (get-config item :target-path))
    (for-each
     (^s (let ((t (build-path (expand-path (get-config item :target-path))
                              (format #f "~a_cut.~a" (path-sans-extension s) (path-extension s)))))
           (unless (file-exists? t)
             (format #t "==> ~a~%" s)
             (cut-and-recompress item s t))))
     (list-source item))))
(define (main args)
  (let ((config (file->sexp-list (cadr args))))
    (set! *default* (assoc-ref config 'default))
    (for-each process-item (map cdr (filter (^x (eq? (car x) 'item)) config))))
  (print "Processed all items.")
  (sys-exit 0))
