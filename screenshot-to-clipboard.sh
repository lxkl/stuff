#!/bin/sh
## Copyright 2023 Lasse Kliemann <lasse@lassekliemann.de>
## MIT License
## https://gitlab.com/lxkl/stuff
set -e
output_dir_="$(mktemp -d)"
while true; do
    infile_="$(inotifywait . -e close_write -q --format %f)"
    case "${infile_?}" in
        *.png|*.PNG) : ;;
        *) continue ;;
    esac
    rm -f "${output_dir_?}"/output.txt
    while true; do
        { lsof "${infile_?}" > /dev/null ; code_="$?" ; } || :
        case "${code_?}" in
            1) break ;;
            *) sleep 1 ;;
        esac
    done
    case "$(file -b "${infile_?}")" in
        "PNG image data, "*) : ;;
        *) continue ;;
    esac
    tesseract -l eng "${infile_?}" "${output_dir_?}"/output
    xclip -selection clipboard -i < "${output_dir_?}"/output.txt
    num_chars_="$(wc -c "${output_dir_?}"/output.txt | awk '{print $1}')"
    echo "Number of recognized characters is ${num_chars_?}"
    if test "${num_chars_?}" -gt 40; then
        text_="$(head -c 40 "${output_dir_?}"/output.txt) ..."
    else
        text_="$(cat "${output_dir_?}"/output.txt)"
    fi
    notify-send "copied: ${text_?}"
done
