import re, sys

def number_leading_whitespace_chars(s):
	match = re.match(r'\s*', s)
	return len(match.group(0)) if match else 0

def filter_permissions(input_file, output_file):
	inside_file = False
	saved_line = None
	indentation = None
	with open(input_file, 'r') as infile, open(output_file, 'w') as outfile:
		for line in infile:
			line = line.rstrip('\n')
			if not line:
				if saved_line is not None:
					print(f"ERROR: unprocessed saved_line: {saved_line}")
					saved_line = None
				inside_file = False
				indentation = None
			elif not inside_file and not re.match(r'^\s', line):
				if saved_line is not None:
					print(f"ERROR: unprocessed saved_line: {saved_line}")
				saved_line = line
				inside_file = True
			elif inside_file and not re.match(r'^\s', line):
				print(f"ERROR: indented line expected: {line}")
			elif inside_file:
				if indentation is None:
					indentation = number_leading_whitespace_chars(line)
				if number_leading_whitespace_chars(line) != indentation:
					print(f"ERROR: wrong indentation: {line}")
				if saved_line is not None:
					outfile.write(saved_line[indentation:] + '\n')
					saved_line = None
				outfile.write(line[indentation:] + '\n')
			else:
				print(f"ERROR: uncaught case: {line}")
		if saved_line is not None:
			print(f"ERROR: unprocessed saved_line: {saved_line}")

if __name__ == "__main__":
	filter_permissions(sys.argv[1], sys.argv[2])
