#!/bin/sh
grep -v '^#' /etc/modules | grep -v '^ *$' | while IFS= read -r i_; do
    modprobe ${i_?}
done
