// Copyright 2023 Lasse Kliemann <lasse@lassekliemann.de>
// MIT License
// https://gitlab.com/lxkl/stuff


// ------------------------------ //
// Simple vertical rhythm in SCSS //
// ------------------------------ //

// Based on: https://www.youtube.com/watch?v=IbfsvI6dh4U
//
// Usage example (including grid):
//
//   @import "rhythm";
//   body {
//     @include rhythm-config-line-height;
//     @include rhythm-config-all-headings;
//     @include rhythm-config-blocks;
//   }
//   @include rhythm-config-flex-grid("body", 16, 1000vh);
//
// Beware that many settings for list-style-type, such as disc or
// circle, might mess with vertical rhythm. I always reset my CSS
// first with this:
// https://gitlab.com/lxkl/stuff/-/raw/main/reset.scss
//
// Here is an example how to get bullets back after that:
//
//   $bullet-width: 1em;
//   li { padding-left: $bullet-width; }
//   li::before {
//     content: "•";
//     display: inline-block;
//     width: $bullet-width;
//     margin-left: -$bullet-width;
//   }
//
// See comments below for more instructions and details.


//                         //
// Configuration variables //
//                         //

// Standard line height in REM.
$rhythm-LINE-HEIGHT-REM: 1.5 !default;

// This many heading levels will be supported. Default is 4, that is,
// h1 to h4.
$rhythm-HEADING-MAX: 4 !default;

// Font size between levels differs by this factor.
$rhythm-HEADING-BASE: 1.25 !default;

// These two parameters control height and vertical adjustment of
// headings. See mixin rhythm-config-heading below for details.
$rhythm-HEADING-THRESHOLD: 0.4 !default;
$rhythm-HEADING-TOP: 0.7 !default;


//                    //
// Internal functions //
//                    //

// Power with integer exponent.
@function rhythm--pow($base, $exponent) {
    $res: 1;
    @for $i from 1 through $exponent { $res: $res * $base; }
    @return $res;
}

// Scale factor for font size of heading level $n.
@function rhythm--heading-scale($n) {
    @return rhythm--pow($rhythm-HEADING-BASE, $rhythm-HEADING-MAX - $n + 1);
}


//                           //
// User functions and mixins //
//                           //

// Convert number of lines to their height in REM.
@function rhythm-height-of-lines($k) {
    @return $k * $rhythm-LINE-HEIGHT-REM * 1rem;
}

// Configure heading level $n.
//
// Font size is the root font size scaled by
// rhythm--heading-scale($n).
//
// Bottom margin is always one line. This is in order that rhythm is
// kept when this bottom margin hits the top margin of another element
// (--> margin collapsing). Another condition must be met for this to
// work, namely the top margin of the following element must be zero
// or at least one line. Proper top margins are ensured for all our
// headings.
//
// Next we determine the total number of lines used for line height
// and top margin ($num-lines). To this end, we round up the scale
// factor ($ceil). If the rounding error ($err) is below
// $rhythm-HEADING-THRESHOLD, we add one more line.
//
// Finally, we distribute $num-lines across top margin and line
// height; a fraction of $rhythm-HEADING-TOP is used for the top
// margin.
//
// How to configure $rhythm-HEADING-THRESHOLD and $rhythm-HEADING-TOP?
// Rhythm will be OK no matter what (I hope so), provided that SCSS
// compilation does not throw any errors. But not all combinations
// will give an aesthetically pleasing result.  Typically, we will
// chose $rhythm-HEADING-THRESHOLD < 0.5 and $rhythm-HEADING-TOP >
// 0.5. The default values work fine in my opinion.
@mixin rhythm-config-heading($n) {
    font-size: rhythm--heading-scale($n) * 1rem;
    margin-bottom: rhythm-height-of-lines(1);
    //
    $ceil: ceil(rhythm--heading-scale($n));
    $err: $ceil - rhythm--heading-scale($n);
    $num-lines: 0;
    @if $err < $rhythm-HEADING-THRESHOLD {
        $num-lines: 1 + $ceil;
    } @else {
        $num-lines: 0 + $ceil;
    }
    $margin-top-lines: $rhythm-HEADING-TOP * $num-lines;
    @if $margin-top-lines < 1 { @error "Top margin should have at least one line!" }
    margin-top:    rhythm-height-of-lines($margin-top-lines);
    line-height:   rhythm-height-of-lines((1 - $rhythm-HEADING-TOP) * $num-lines);
}

// Configure standard line height.
@mixin rhythm-config-line-height() {
    line-height: rhythm-height-of-lines(1);
}

// Configure headings level 1 to max.
@mixin rhythm-config-all-headings() {
    @for $n from 1 through $rhythm-HEADING-MAX {
        h#{$n} { @include rhythm-config-heading($n); }
    }
}

// Configure common block elements.
@mixin rhythm-config-blocks() {
    p, blockquote, pre, li { margin-bottom: rhythm-height-of-lines(1); }
}

// Paint a grid in the background.
//
// $font-size-px: base font size in px. It must be provided, since
// there is no way to deduce this.
@mixin rhythm-config-simple-grid($font-size-px) {
    $line-height-px: $font-size-px * $rhythm-LINE-HEIGHT-REM;
    background-image: linear-gradient(to bottom, red 1px, transparent 1px);
    background-size: 100% ($line-height-px * 1px);
}

// Paint a grid in the background of a flex container so that the grid
// scrolls with the content. Arguments:
//
// $container: the flex container, for example, "div#content".
//
// $font-size-px: base font size in px. It must be provided, since
// there is no way to deduce this.
//
// $height: there seems to be no way to deduce the height of the
// content in the container. So we have to provide something large
// here, such as, 1000vh.
@mixin rhythm-config-flex-grid($container, $font-size-px, $height) {
    $line-height-px: $font-size-px * $rhythm-LINE-HEIGHT-REM;
    #{$container}::before {
        content: "";
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 1;
        height: $height;
        pointer-events: none;
        background: repeating-linear-gradient(
            to bottom,
            transparent,
            transparent ($line-height-px - 1) * 1px,
            red ($line-height-px - 1) * 1px,
            red $line-height-px * 1px
        );
    }
    #{$container} > * {
        position: relative;
        z-index: 2;
    }
}
